import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: []
})
export class AppComponent {
  public title;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router
  ){
    this.title = 'Angular Rocks';
  }

  ngOnInit(){
  }
}
