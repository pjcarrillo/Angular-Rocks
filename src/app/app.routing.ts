import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// import components
import { BarComponent } from './bar/bar.component';
import { FooComponent } from './foo/foo.component';
import { HomeComponent } from './home/home.component';
import { QueenComponent } from './queen/queen.component';
import { TheBeatlesComponent } from './the-beatles/the-beatles.component';
import { TheRollingStonesComponent } from './the-rolling-stones/the-rolling-stones.component';

const appRoutes: Routes = [
	{path: 'foo', component: FooComponent},
	{path: 'bar', component: BarComponent},
	{path: '', component: HomeComponent},
	{path: 'home', component: HomeComponent},
	{path: 'the-rolling-stones', component: TheRollingStonesComponent},
	{path: 'the-beatles', component: TheBeatlesComponent},
	{path: 'queen', component: QueenComponent}
];

export const AppRoutingProviders: any[] = [];
export const Routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
