import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { Routing, AppRoutingProviders } from './app.routing';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { FooComponent } from './foo/foo.component';
import { BarComponent } from './bar/bar.component';
import { TheRollingStonesComponent } from './the-rolling-stones/the-rolling-stones.component';
import { TheBeatlesComponent } from './the-beatles/the-beatles.component';
import { QueenComponent } from './queen/queen.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooComponent,
    BarComponent,
    TheRollingStonesComponent,
    TheBeatlesComponent,
    QueenComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    Routing
  ],
  providers: [AppRoutingProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
